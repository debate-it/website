import type { PageServerLoad } from "./$types";

import { fail, redirect, type Actions, error } from "@sveltejs/kit";


import { client } from '$lib/server/graphql';
import { gql } from "@urql/core";


const verifyResetRequestQuery = /* GraphQl */ gql`
  query vertifyPasswordReset($id: String!) {
    verifyPasswordReset(id: $id)
  }
`;

const resetPasswordQuery = /* GraphQl */ gql`
  mutation resetPassword($id: String!, $password: String!) {
    resetPassword(id: $id, password: $password)
  }
`;

export const load = (async (event) => {
  const id = event.params['id'];
 
  const result = await client.query(verifyResetRequestQuery, { id }).toPromise();
 
  if (result.error) {
    throw error(404, { message: result.error.message });
  }

}) satisfies PageServerLoad;


export const actions = {
  default: async ({ request, params }) => {
    const { id } = params;
    const data = await request.formData();

    const password = data.get('password');
    const confirmPassword = data.get('confirmPassword');
   
    if (!password || !confirmPassword) {
      throw fail(400, { message: 'New password not provided' });
    }
   
    if (password.length === 0 || confirmPassword.length === 0) {
      throw fail(400, { message: 'Password should not be empty' })
    } 
    
    if (password !== confirmPassword) {
      throw fail(400, { message: 'Password doesn\'t match' })
    }
      
    const result = await client.mutation(resetPasswordQuery, { id, password }).toPromise();

    if (result.error) {
      throw fail(500, { message: result.error.message });
    } 
    
    return { success: result.data.resetPassword };
  }
} satisfies Actions;