import { env } from '$env/dynamic/private';

import { Client, fetchExchange } from '@urql/core';

export const client = new Client({
  url: env.BACKEND_URL,
  exchanges: [fetchExchange]
});