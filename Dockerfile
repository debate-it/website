FROM node:18-alpine AS builder
WORKDIR /app
COPY package.json yarn.lock .
RUN yarn install
COPY . .
RUN yarn build


FROM node:18-alpine
WORKDIR /app

COPY --from=builder /app/build build/
COPY --from=builder /app/node_modules node_modules/

COPY package.json .

EXPOSE 80 

ENV NODE_ENV=production
ENV PORT=80

CMD [ "node", "build" ]