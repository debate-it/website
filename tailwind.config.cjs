/** @type {import('tailwindcss').Config}*/
const config = {
	content: ['./src/**/*.{html,js,svelte,ts}'],

	theme: {
		fontFamily: {
			display: ['Raleway', 'system-ui', 'sans-serif'],
      body: ['Montserrat', 'Roboto', 'sans-serif']
		},	
		extend: {
			colors: {
        primary: '#d12020',
        secondary: '#4cadd4',

        foreground: '#dbbc73',
        background: '#393936',
        surface: '#282826'
      }
		}
	},

	plugins: [
		require('@tailwindcss/forms'),
		require('@tailwindcss/typography'),
	]
};

module.exports = config;
